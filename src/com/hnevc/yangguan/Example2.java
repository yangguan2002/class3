//package com.hnevc.yangguan;
//
//public class Example2 {
//    //if()else else if().......
//    //根据1234567判断当前星期几？
//    public static void main(String[] args) {
//        int week = 1 ;
//        if (week == 1 ){
//            System.out.println("星期一");
//        }else if (week == 2){
//            System.out.println("星期二");
//        }else if (week == 3 ){
//            System.out.println("星期三");
//        }else if (week == 4 ){
//            System.out.println("星期四");
//        }else if (week == 5 ){
//            System.out.println("星期五");
//        }
//    }
//}

public class Example2 {
    public static void main(String[] args) {
        int week = 4;
        switch (week){
            case 1:
                System.out.println("星期一");
                break;
            case 2:
                System.out.println("星期二");
                break;
            case 3:
                System.out.println("星期三");
                break;
            case 4:
                System.out.println("星期四");
                break;
            case 5:
                System.out.println("星期五");
                break;
            default:
                System.out.println("输入的数值有误！");

        }

    }

}